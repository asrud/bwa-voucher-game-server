const mongoose = require('mongoose');
let bankSchema = mongoose.Schema(
  {
    name: {
      type: String,
      require: [true, 'Nama pemilik harus diisi'],
    },
    bankName: {
      type: String,
      require: [true, 'Nama bank harus diisi'],
    },
    noAccount: {
      type: Number,
      default: 0,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model('Bank', bankSchema);
