const mongoose = require('mongoose');
let transactionSchema = mongoose.Schema(
  {
    historyVoucherTopUp: {
      gameName: { type: String, require: [true, 'nama game harus diisi'] },
      category: { type: String, require: [true, 'kategori harus diisi'] },
      thumbnail: { type: String },
      cointName: { type: String, require: [true, 'nama koin harus diisi'] },
      cointQty: { type: String, require: [true, 'jumlah koin harus diisi'] },
      price: { type: Number },
    },
    historyPayment: {
      name: { type: String, require: [true, 'nama harus diisi'] },
      type: { type: String, require: [true, 'tipe pembayaran harus diisi'] },
      bankName: { type: String, require: [true, 'nama bank harus diisi'] },
      noAccount: { type: String, require: [true, 'no rekening harus diisi'] },
    },
    name: {
      type: String,
      require: [true, 'nama harus diisi'],
      maxLength: [225, 'panjang nama harus 3 - 255 karater'],
      minLength: [3, 'panjang nama harus 3 - 255 karater'],
    },
    accountUser: {
      type: String,
      require: [true, 'nama akun harus diisi'],
      maxLength: [225, 'panjang nama harus 3 - 255 karater'],
      minLength: [3, 'panjang nama harus 3 - 255 karater'],
    },
    tax: {
      type: Number,
      default: 0,
    },
    value: {
      type: Number,
      default: 0,
    },
    status: {
      type: String,
      enum: ['pending', 'success', 'failed'],
      default: 'pending',
    },
    player: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Player',
    },
    historyUser: {
      name: { type: String, require: [true, 'nama player harus diisi'] },
      phoneNumber: {
        type: Number,
        require: [true, 'nama player harus diisi'],
        maxLength: [13, 'panjang nama harus 9 - 13 karater'],
        minLength: [9, 'panjang nama harus 9 - 13 karater'],
      },
    },
    category: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Category',
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model('Transaction', transactionSchema);
